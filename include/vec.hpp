#ifndef __VEC_HPP__
#define __VEC_HPP__ 1

namespace vec {

template<typename _Dt>
struct vec3 {
    _Dt x; _Dt y; _Dt z;
};

template<typename _Dt>
inline vec3<_Dt> operator+ (vec3<_Dt> lhs, const vec3<_Dt>& rhs) {
    lhs.x += rhs.x; lhs.y += rhs.y; lhs.z += rhs.z;
    return lhs;
}

template<typename _Dt>
inline vec3<_Dt> operator- (vec3<_Dt> lhs, const vec3<_Dt>& rhs) {
    lhs.x -= rhs.x; lhs.y -= rhs.y; lhs.z -= rhs.z;
    return lhs;
}

template<typename _Dt>
inline vec3<_Dt> operator* (vec3<_Dt> lhs, const double rhs) {
    lhs.x *= rhs; lhs.y *= rhs; lhs.z *= rhs;
    return lhs;
}

template<typename _Dt>
inline vec3<_Dt> operator* (const double lhs, vec3<_Dt> rhs) {
    rhs.x *= lhs; rhs.y *= lhs; rhs.z *= lhs;
    return rhs;
}

template<typename _Dt>
inline bool operator== (const vec3<_Dt>& lhs, const vec3<_Dt>& rhs) {
    return (lhs.x == rhs.x) && (lhs.y == rhs.y) && (lhs.z == rhs.z);
}

template<typename _Dt>
inline bool operator!= (const vec3<_Dt>& lhs, const vec3<_Dt>& rhs) {
    return !(operator==(lhs, rhs));
}

template<typename _Dt>
std::ostream& operator<< (std::ostream& out, const vec3<_Dt>& pt) {
    out << "vec3 (" 
        << pt.x << ", "
        << pt.y << ", "
        << pt.z << ")";
    return out;
}

}; // vec

#endif