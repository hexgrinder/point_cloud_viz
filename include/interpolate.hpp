#ifndef __INTERPOLATE_HPP__
#define __INTERPOLATE_HPP__ 1

#include <assert.h>

namespace interpolate {
    
/**
   Defines a linear interpolation between two vertices, each vertice with 
   their own scalar value.
*/
template <typename _Vt>
struct Linear {

    Linear(double limit) 
        : interp_limit(limit) { assert(0 < limit); };
    
    const double interp_limit;
    
    void operator()(
        const double& pivot, 
        const _Vt& p1, const _Vt& p2, 
        const double& valp1, const double& valp2,
        _Vt& result) {
       
        if (fabs(pivot - valp1) < interp_limit) { result = p1; return; }
        if (fabs(pivot - valp2) < interp_limit) { result = p2; return; }
        if (fabs(valp1 - valp2) < interp_limit) { result = p1; return; }

        double mu = (pivot - valp1) / (valp2 - valp1);
        result = p1 + ((p2 - p1) * mu);
    };
};

}; // interpolate

#endif