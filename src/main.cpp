//#define __DEBUG__ 1

// drawing
#include <memory>
#include <vector>

#include "marching_cubes.hpp"
#include "interpolate.hpp"
#include "glut_frame.hpp"
#include "fio.hpp"

#define __INTERP_LIMIT__ 0.0001

bool shell = true;
bool lines = false;
double step = .285; //.975;

glfw::GlutSkeleton<marching_cubes::XYZ> frmwrk;
    
interpolate::Linear<marching_cubes::XYZ> linear_interpf(__INTERP_LIMIT__);
marching_cubes::SurfaceBuilder builder;

fio::fifile_3D<unsigned char, 256,256,256> data("data/foot.raw"); 

/**
    Converts discrete data set to a continuous function that
    marching cube is expecting.
*/
struct foot {    
    
#define CLAMP(a,v,b) (v >= a) ? ((v <= b) ? v : b) : a  

    double operator() (double x, double y, double z) {
        return static_cast<double>(
            data.get(
                static_cast<unsigned long>(CLAMP(0,x,(data.elms_max - 1))),
                static_cast<unsigned long>(CLAMP(0,y,(data.rows_max - 1))),
                static_cast<unsigned long>(CLAMP(0,z,(data.page_max - 1))) ));
    };
};

inline bool threshold_comparef(const double val, const double b) { 
    return val < b; 
}

// *********


void keyboard(unsigned char key, int x, int y) {
    switch (key) {
        // ESC key
        case 27:    
            exit (0);
            break;
        case '=':
            frmwrk.camera.move(0,0,-1);
            glutPostRedisplay();
            break;
        case '-':
            frmwrk.camera.move(0,0,1);
            glutPostRedisplay();
            break;
        case '[':
            if (step <= 10.0) {
                step += .005;
                glutPostRedisplay();    
            }
            std::cout << step << std::endl;
            break;
        case ']':
            if (step > .005) {
                step -= .005;
                glutPostRedisplay();
            }
            std::cout << step << std::endl;
            break;
        case 's':
            shell = !shell;
            glutPostRedisplay(); 
            break;
        case 'l':
            lines = !lines;
            glutPostRedisplay(); 
            break;
        case 'c':
            glutPostRedisplay();
            break;
        case 'C':
            glutPostRedisplay();
            break;
        default:           
        break;
    }
}

void specialkeyboard(int key, int x, int y) {
    switch (key) {
        case GLUT_KEY_LEFT:
            frmwrk.camera.move(1,0,0);
            break;
        case GLUT_KEY_RIGHT:
            frmwrk.camera.move(-1,0,0);
            break;
        case GLUT_KEY_UP:
            frmwrk.camera.move(0,1,0);
            break; 
        case GLUT_KEY_DOWN:
            frmwrk.camera.move(0,-1,0);
            break;
        default:
            break;
    }
    glutPostRedisplay();
}

void display(void) {

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    gluLookAt(
        265, //frmwrk.camera.pos.x, 
        265, //frmwrk.camera.pos.y, 
        265, //frmwrk.camera.pos.z,
        frmwrk.camera.look_at.x, 
        frmwrk.camera.look_at.y, 
        frmwrk.camera.look_at.z,
        frmwrk.camera.up.x, 
        frmwrk.camera.up.y, 
        frmwrk.camera.up.z
    );

    glEnable(GL_NORMALIZE);
    glEnable(GL_BLEND);
    
    // updating the lighting here maintains the light in a static 
    // position while flying around the screen.
    GLfloat lightPos[4] = {0,0,5.0,0.0};
    glLightfv(GL_LIGHT0,GL_POSITION,(GLfloat *) &lightPos);
  
    // drawing stuff below...
    glfw::axis();

    glColor3f (0.4f, 0.4f, 0.4f);
    glLineWidth(1);
    glfw::grid3D(1,1,1, 1,1,1);

    marching_cubes::TriangleList triangles;
    
    foot shape;
    
    triangles.clear();
    
    double val;
    
    glPushMatrix();
    //glTranslatef(0,-128,0);
    //glRotatef(180.0f, 0.0,1.0,0.0);
    //glTranslatef(128,128,0);
            
    // bone
    for (int z = 0; z < data.page_max; z += 1) {
    for (int y = 0; y < data.rows_max; y += 1) {
    for (int x = 0; x < data.elms_max; x += 1) {
        
        val = shape(x,y,z);

        // 120 - 235 : bone
        if (120 < val && val < 235) {         
            builder.polygonize(
                x, y, z, 
                1, 
                120, 
                threshold_comparef,
                linear_interpf, shape, 
                triangles);
        }
    }
    }
    }
    
    for (auto triangle : triangles) {
        if (shell) { 
            glBegin(GL_TRIANGLES);
            glColor4f(.75f, .75f, 1.0f, 1.0f);
            for (int i = 0; i < 3; ++i) {
                glNormal3d(
                    triangle.normals[i].x,
                    triangle.normals[i].y,
                    triangle.normals[i].z);
                glVertex3d(
                    triangle.p[i].x, 
                    triangle.p[i].y, 
                    triangle.p[i].z );
            }
            glEnd();
        }
        
        if (lines) {
            glDisable(GL_LIGHTING);
            glBegin(GL_LINE_LOOP);
            glLineWidth(3);
            glColor3d(0.3,0,0);
            for (int i = 0; i < 3; ++i) {
                glVertex3d(
                    triangle.p[i].x, 
                    triangle.p[i].y, 
                    triangle.p[i].z );
            }
            glEnd();
            glEnable(GL_LIGHTING);        
        }
    }

    triangles.clear();
    
    // skin
    
    for (int z = 0; z < data.page_max; z += 2) {
    for (int y = 0; y < data.rows_max; y += 2) {
    for (int x = 0; x < data.elms_max; x += 2) {
        
        val = shape(x,y,z);

        // 12 - 110 : skin
        // 120 - 235 : bone
        if (15 < val && val < 110) {         
            builder.polygonize(
                x, y, z, 
                2, 
                15, 
                threshold_comparef,
                linear_interpf, shape, 
                triangles);
        }
    }
    }
    }
    
    for (auto triangle : triangles) {
        if (shell) { 
            glBegin(GL_TRIANGLES);
            glColor4f(.95,.95,0,0.25f);
            for (int i = 0; i < 3; ++i) {
                glNormal3d(
                    triangle.normals[i].x,
                    triangle.normals[i].y,
                    triangle.normals[i].z);
                glVertex3d(
                    triangle.p[i].x, 
                    triangle.p[i].y, 
                    triangle.p[i].z );
            }
            glEnd();
        }
        
        if (lines) {
            glDisable(GL_LIGHTING);
            glBegin(GL_LINE_LOOP);
            glLineWidth(3);
            glColor3d(0.3,0,0);
            for (int i = 0; i < 3; ++i) {
                glVertex3d(
                    triangle.p[i].x, 
                    triangle.p[i].y, 
                    triangle.p[i].z );
            }
            glEnd();
            glEnable(GL_LIGHTING);        
        }
    }
    
    glPopMatrix();
    
    glutSwapBuffers();
}

int main (int argc, char *argv[]) {
    frmwrk.displayf = display;
    frmwrk.keyboardf = keyboard;
    frmwrk.speckeyboardf = specialkeyboard;
    frmwrk.start(argc, argv);
}
