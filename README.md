# Point Cloud Visualization #

A point cloud is a map of 3-d coordinates, each tagged with some sort of data/metadata describing that particular point.

This project explores an implementation of the marching cubes algorithm to construct a mesh onto a point cloud with varying scalar densities.

### The Marching Cubes Algorithm ###

The marching cubes algorithm is a triangular-mesh generator, which uses interpolated scalar values to define an iso-surface separating quantities along a threshold. 

The original algorithm was created by William Lorensen and Harvey Cline at General Electric.  Their paper, ["Marching Cubes: A High Resolution 3D Surface Construction Algorithm" (1987)](http://dl.acm.org/citation.cfm?doid=37402.37422), presented an efficient means of approximating surfaces from 3D grid data.

The algorithm used in the project is based on an implementation provided in [Paul Bourke's](http://paulbourke.net/geometry/polygonise/) work (Accessed Feb 2016).  

### Methodology ###

* The algorithm was implemented in C++ (C++0x).
* The OpenGL library was used to generate the graphical display.
* A 3D scan (256 x 256 x 256 data points) depicting the toe region of a human left foot was provided for testing. The .raw data file contains byte-aligned, scalar values (0 - 255) representing the density at a given xyz-coordinate.

### Initial Results ###

![Figure 1 - Bone and skin rendering.](./img/foot.PNG)  

**Figure 1** illustrates the result of a single-pass rendering using the marching cubes algorithm. The rendering separately screened for bone density (120 - 235) and skin density (10 - 110). The skin rendering was intentionally made transparent so the bone structure can be viewed.  

*****  

![Figure 2 - Bone rendering.](./img/foot_bone.PNG)  


**Figure 2** illustrates the result of a single-pass rendering using the marching cubes algorithm.  The rendering was screened for density values between 120 and 235.


### **OBS**ervations ###

1. Figure 1 displays a jagged representation of the skin rendering. Possibly due to varying discontinuities in the data? Perhaps I am omitting data?

2. Rendering is slow. This was especially true for Figure 1 (~5min). This naive implementation scans through the entire data set to generate the mesh. This creates unnecessary surfaces eventually obscured by other surfaces closer to the eye point. 

### Future Work ###

* RE:OBS(1) - It is suggested that the data be "smoothed" or averaged prior to rendering.

* RE:OBS(2) - If we are to speed up rendering, only un-obscured cubes must be passed to the marching cubes algorithm for iso-surface generation. (NOTE: Requires adjustment for displaying transparent layers.)